# Translation of plinth debconf templates to Spanish
# Copyright (C) 2019 FreedomBox packaging team <freedombox-pkg-team@lists.alioth.debian.org>
# This file is distributed under the same license as the plinth package.
# Fioddor Superconcentrado <fioddor@gmail.com>, 2019
#
msgid ""
msgstr ""
"Project-Id-Version: plinth 19.20\n"
"Report-Msgid-Bugs-To: plinth@packages.debian.org\n"
"POT-Creation-Date: 2019-11-18 18:11-0500\n"
"PO-Revision-Date: 2019-10-30 12:45+0100\n"
"Last-Translator: Fioddor Superconcentrado <fioddor@gmail.com>\n"
"Language-Team: Debian L10n Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.1.1\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. Type: note
#. Description
#: ../templates:1001
msgid "FreedomBox first wizard secret - ${secret}"
msgstr "Secreto del asistente al primer arranque de FreedomBox - ${secret}"

#. Type: note
#. Description
#: ../templates:1001
#, fuzzy
#| msgid ""
#| "Please save this string. You will be asked to enter this in the first "
#| "screen after you launch the FreedomBox interface. In case you lose it, "
#| "you can find it in the file /var/lib/plinth/firstboot-wizard-secret."
msgid ""
"Please note down the above secret. You will be asked to enter this in the "
"first screen after you launch the FreedomBox web interface. In case you lose "
"it, you can retrieve it by running the following command:"
msgstr ""
"Por favor, anote esta cadena de texto. Se le pedirá en la primera pantalla "
"al lanzar el interfaz web de FreedomBox. En caso de pérdida puede "
"recuperarla mirando el fichero /var/lib/plinth/firstboot-wizard-secret."
